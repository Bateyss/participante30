package Controlador;

import Conexion.Conexion;
import DAO.CompetenciasDao;
import DAO.UsuariosDao;
import Modelo.Competencias;
import Modelo.Usuarios;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UsuariosServlet extends HttpServlet {

    // Atributos de coneion a la BD
    private Conexion con;
    // Atributos de Respuesta al usuario e impresion
    public RequestDispatcher rd;
    private String msg;
    // Atributos de comprobacion
    private boolean respuesta;
    // Atributos de Entidades
    private Competencias competencias;
    private Usuarios usuarios;
    // Atributos de manejo CRUD
    private CompetenciasDao competenciasDao;
    private UsuariosDao usuariosDao;
    // atributo de reusabilidad del codigo;
    private final String carpera = ""; //Es la Carpeta donde se encuentra la vista
    private final String id_tabla = ""; //Es el nombre de la entrada de datos segun el id de la tabla utilziada
    private int id; // Es el atributo para utilizar el valor de la entrada de datos de id_tabla
    // Atributos de Impresion en la vista
    List<Usuarios> listaUsuarios = new LinkedList<>();
    List<Competencias> listaCompetencias = new LinkedList<>();

    protected void errorHandler(HttpServletRequest request, HttpServletResponse response, Exception exception)
            throws ServletException, IOException { // Este metodo permitira capturar el error encontrado
        System.out.println("Error en " + exception.getMessage());
        rd = request.getRequestDispatcher("/equipo1/Resources/"+carpera+"tabla.jsp");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action"); //Se recibe un paraetro de la vista con un valor String
        switch (action) {
            case "insertar": ;
                break;
            case "actualizar": ;
                break;
            case "eliminar": ;
                break;
            case "consultarId": ;
                break;
            case "consultar": ;
                break;
            default: ;
                break;
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //recepcion de datos de la vista
            id = 0;
            String password = request.getParameter("pass");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String correo = request.getParameter("correo");
            int id_competencias =  Integer.parseInt(request.getParameter("competencia"));
            // se inicializan las entidades para enviar datos
            competencias = new Competencias(id_competencias);
            usuarios = new Usuarios(id);
            // se adjuntan los datos al entiti
            usuarios.setPass(password);
            usuarios.setNombre(nombre);
            usuarios.setApellido(apellido);
            usuarios.setCorreo(correo);
            usuarios.setCompetencias(competencias);
            // se inicia la conexion
            con = new Conexion();
            // se inicia y ejecuta la accion DAO
            usuariosDao = new UsuariosDao(con);
            this.respuesta = this.usuariosDao.insertar(this.usuarios);
            if (respuesta) {
                this.msg = "Insertado Correctamente";
            }else{
            this.msg = "Ocurrio un error al insertar";
            }
            // se envian los datos en forma de impresion de parametros
            request.setAttribute("msg", msg);
            // se direcciona a la vista y se envian los datos
            rd = request.getRequestDispatcher("/equipo1/Resources/"+carpera+"tabla.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Error al Insertar en: "+ this.getServletName());
            this.errorHandler(request, response, e);
        }
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void consultarId(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
