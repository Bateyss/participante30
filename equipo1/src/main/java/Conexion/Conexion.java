package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    Connection con = null;
    private final String bd = "equipo1";
    private final String user = "kz";
    private final String pass = "kzroot";
    private final String url = "jdbc:mysql://usam-sql.sv.cds:3306/" + bd + "?useSSL=false";

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
            if (con != null) {
                System.out.println("Conexion Exitosa");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error al conectar BD");
            System.out.println("Error en: " + e);
        }
    }

    public Connection conectar() {
        return con;
    }
    public void deconectar(){
        try {
            this.con.close();
        } catch (SQLException e) {
            System.out.println("Error al desconectar BD");
            System.out.println("Error en: " + e);
        }
    }
}
