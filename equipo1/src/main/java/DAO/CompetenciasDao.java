package DAO;

import Conexion.Conexion;
import Modelo.Competencias;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cesar.murciausam
 */
public class CompetenciasDao extends Conexion {

    Conexion con;

    public CompetenciasDao(Conexion con) {
        this.con = con;
    }

    public boolean insertar(Competencias c) {
        try {
            String sql = "insert into competencias values(id_competencia=?, nombre=?,descripcion=?)";
            PreparedStatement ps = conectar().prepareStatement(sql);
            ps.setInt(1, c.getId_competencia());
            ps.setString(2, c.getNombre());
            ps.setString(3, c.getDescripcion());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean actualizar(Competencias c) {
        try {
            String sql = "update competencias set nombre=?, descripcion=? where id_competencia=?";
            PreparedStatement ps = conectar().prepareStatement(sql);
            ps.setString(1, c.getNombre());
            ps.setString(2, c.getDescripcion());
            ps.setInt(3, c.getId_competencia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public List<Competencias> listar_competencias() {
        try {
            String query = "select * from competencias";
            PreparedStatement ps = conectar().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            List<Competencias> lista = new LinkedList<>();
            while (rs.next()) {
                Competencias con = new Competencias(rs.getInt("id_competencia"));
                con.setNombre(rs.getString("nombre"));
                con.setDescripcion(rs.getString("descripcion"));
                lista.add(con);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Competencias> listarxid(Competencias c) {
        try {
            String query = "select * from competencias where id_competencia=?";
            PreparedStatement ps = conectar().prepareStatement(query);
            ps.setInt(1, c.getId_competencia());
            ResultSet rs = ps.executeQuery();
            List<Competencias> lista = new LinkedList<>();
            while (rs.next()) {
                Competencias con = new Competencias(rs.getInt("id_competencia"));
                con.setNombre(rs.getString("nombre"));
                con.setDescripcion(rs.getString("descripcion"));
                lista.add(con);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(Competencias c) {
        try {
            String sql = "delete from competencias where id_competencia=?";
            PreparedStatement ps = conectar().prepareStatement(sql);
            ps.setInt(1, c.getId_competencia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

}
