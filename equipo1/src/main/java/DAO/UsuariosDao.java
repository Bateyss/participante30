/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import Modelo.Competencias;
import Modelo.Usuarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cesar.murciausam
 */
public class UsuariosDao {

    Conexion con;

    public UsuariosDao(Conexion con) {
        this.con = con;
    }

    public boolean insertar(Usuarios u) {
        try {
            String sql = "insert into usuario values(id_usuario=?, pass=?, nombre=?, apellido=?, correo=?, id_competencia=?)";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            Competencias c = u.getCompetencias();
            ps.setInt(1, u.getId_usuario());
            ps.setString(2, u.getPass());
            ps.setString(3, u.getNombre());
            ps.setString(4, u.getApellido());
            ps.setString(5, u.getCorreo());
            ps.setInt(6, c.getId_competencia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean actualizar(Usuarios u) {
        try {
            String sql = "update competencias set pass=?, nombre=?, apellido=?, correo=?, id_competencia=? where id_usuario=?";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            Competencias c = u.getCompetencias();
            ps.setString(1, u.getPass());
            ps.setString(2, u.getNombre());
            ps.setString(3, u.getApellido());
            ps.setString(4, u.getCorreo());
            ps.setInt(5, c.getId_competencia());
            ps.setInt(6, u.getId_usuario());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public List<Usuarios> listar_competencias() {
        try {
            String query = "select * from usuario";
            PreparedStatement ps = con.conectar().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            List<Usuarios> lista = new LinkedList<>();
            while (rs.next()) {
                Usuarios us = new Usuarios(rs.getInt("id_usuario"));
                Competencias c = us.getCompetencias();
                us.setPass(rs.getString("pass"));
                us.setNombre(rs.getString("nombre"));
                us.setApellido(rs.getString("apellido"));
                us.setCorreo(rs.getString("correo"));
                c.setNombre(rs.getString("nombre"));
                us.setCompetencias(c);
                lista.add(us);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Usuarios> listarxid(Usuarios u) {
        try {
            String query = "select * from usuario where id_usuario=?";
            PreparedStatement ps = con.conectar().prepareStatement(query);
            ps.setInt(1, u.getId_usuario());
            ResultSet rs = ps.executeQuery();
            List<Usuarios> lista = new LinkedList<>();
            while (rs.next()) {
                Usuarios us = new Usuarios(rs.getInt("id_usuario"));
                Competencias c = us.getCompetencias();
                us.setPass(rs.getString("pass"));
                us.setNombre(rs.getString("nombre"));
                us.setApellido(rs.getString("apellido"));
                us.setCorreo(rs.getString("correo"));
                c.setNombre(rs.getString("nombre"));
                us.setCompetencias(c);
                lista.add(us);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(Usuarios us) {
        try {
            String sql = "delete from usuario where id_usuario=?";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setInt(1, us.getId_usuario());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

}
