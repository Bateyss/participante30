/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author cesar.murciausam
 */
public class Usuarios {
    private int id_usuario;
    
    private String pass;
    private String nombre;
    private String apellido;
    private String correo;
    private Competencias competencias;

    public Usuarios(int id_usuario) {
        this.id_usuario = id_usuario;
    }
    
    

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Competencias getCompetencias() {
        return competencias;
    }

    public void setCompetencias(Competencias competencias) {
        this.competencias = competencias;
    }
    
    

   
    
    
    
}
