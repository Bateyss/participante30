<%-- 
    Document   : tabla
    Created on : 01-24-2020, 11:22:45 AM
    Author     : luis.peñateusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Competencias</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col m12">
                    <div class="card blue white-text center-align">
                        <h3>Competencias</h3>
                    </div>
                </div>
            </div>
        </div>
        <table class="rensive-table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${competencias}" var="c">
                <tr>
                    <td>${c.getNombre}}</td>
                    <td>${u.getDescripcion}</td>
                    <td>
                        <a href="competencias?action=consultarxid&id=${c.id_competencia}" class="btn blue">Editar</a>
                        <a href="competencias?action=eliminar&id=${c.id_competencia}" class="btn red">Eliminar</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div class="row">
        <div class="col s12 m12">
            <a href="competencias?action=nuevo" class="btn gray" >Nuevo</a>
        </div>
    </div>
</body>
</html>
