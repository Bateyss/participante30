<%-- 
    Document   : competenciasave
    Created on : 01-24-2020, 11:55:31 AM
    Author     : luis.peñateusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <form method="post" action="competencias?action=insertar" >

                <div class="row">
                    <div class="col s12 m12">
                            <div class="card black white-text center z-depth-3"><h1>Datos Competencias</h1></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col m4"></div>
                    <div class="col m4">
                        Nombre:
                        <input type="hidden" name="id_competencia" class="form-control" value="${id_competencia}" />
                        <input type="text" name="nombre" class="form-control" value="${nombre}"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col m4"></div>
                    <div class="col m4">
                        Descripcion:
                        <input type="text" name="descripcion" class="form-control" value="${descripcion}"/>
                    </div>
                </div>


                <div class="row">
                    <div class="col m4"></div>
                    <div class="col m4">
                        <button type="submit" value="Guardar" class="btn gray"/>
                    </div>
                </div>

            </form>
        </div>   
    </body>
</html>
