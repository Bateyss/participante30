<%-- 
    Document   : competenciaupdate
    Created on : 01-24-2020, 11:55:47 AM
    Author     : luis.peñateusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <form method="post" action="competencias?action=validacion" >

                <div class="row">
                    <div class="col s12 m12">
                        <div class="card black white-text center z-depth-3"><h1>Datos Competencias</h1></div>
                    </div>
                </div>

                <c:forEach items="${competencias}" var="c">

                    <div class="row">
                        <div class="col m4"></div>
                        <div class="col m4">
                            Nombre:
                            <input type="hidden" name="id_competencia" class="form-control" value="${c.id_competencia}" />
                            <input type="text" name="nombre" class="form-control" value="${c.nombre}"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col m4"></div>
                        <div class="col m4">
                            Descripcion:
                            <input type="text" name="descripcion" class="form-control" value="${c.descripcion}"/>
                        </div>
                    </div>
                </c:forEach>

                <div class="row">
                    <div class="col m4"></div>
                    <div class="col m4">
                        <input type="submit" value="Guardar" class="btn gray"/>
                    </div>
                </div>

            </form>
        </div>   
    </body>
</html>
