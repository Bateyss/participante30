<%-- 
    Document   : actualizar
    Created on : 01-24-2020, 11:21:16 AM
    Author     : evelyn.andradeusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar</title>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    </head>
    <body>
        <form>
            <c:forEach items="usuario" var="us">
                <div class="container">
                    <div class="row">
                        <div class="col m12">
                            <div class="card blue white-text center-align">
                                <h3>Editar usuario</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col m3">
                        Ingrese Nombre:
                    </div>
                    <div class="col m3">
                        <input type="text" name="nombre" required=""/>
                    </div>
                    <div class="col m3">

                    </div>
                </div>
                <div class="row">
                    <div class="col m3">
                        Ingrese Apellido:
                    </div>
                    <div class="col m3">
                        <input type="text" name="apellido" required=""/>
                    </div>
                    <div class="col m3">

                    </div>
                </div>
                <div class="row">
                    <div class="col m3">
                        Ingrese Correo:
                    </div>
                    <div class="col m3">
                        <input type="text" name="correo" required=""/>
                    </div>
                    <div class="col m3">

                    </div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3"></div>
                    <div class="col m3">
                        <select name="competencia">
                            <option value="">--selecione--</option>
                            <c:forEach items="${competencias}" var="c">
                                <option value="${c.getId_competencia()}">${c.getNombre()}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </c:forEach>
            <a href="UsuarioServlet?action=actualizar" class="btn green white-text z-depth-4">Actualizar</a>
        </form>
    </body>
</html>
