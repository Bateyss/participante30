<%-- 
    Document   : usuario
    Created on : 01-24-2020, 09:49:33 AM
    Author     : evelyn.andradeusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Usuario</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col m12">
                    <div class="card blue white-text center-align">
                        <h3>Usuario</h3>
                    </div>
                </div>
            </div>
        </div>
        
        <table class="rensive-table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Correo</th>
                    <th>Competencia</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${usuarios}" var="u">
                <tr>
                    <td>${u.getNombre()}</td>
                    <td>${u.getApellido()}</td>
                    <td>${u.getCorreo()}</td>
                    <td>${u.getCompetecias().getNombre()}</td>
                </tr>
                
            <a href="UsuarioServlet?action=eliminar&" class="btn red white-text z-depth-4">Eliminar</a>
            <a href="UsuarioServlet?action=actualizar&" class="btn green white-text z-depth-4">Actualizar</a>
            </c:forEach>
        </tbody>
    </table>
        <a href="UsuarioServlet?action=insertar" class="btn green white-text z-depth-4">Nuevo Usuario</a>
</body>
</html>
